<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;

use DB;


class RegistrationController extends Controller
{



    /**
     * Registration Module
     *  Compose of all request for Registration
     *
     * @author sonny.parungao
     * @date 08252016
     *
     */


    public function processAddUser(Request $request) {

        $input = $request->all();

        $input['firstname'] =$input['firstname'];
        $input['lastname'] = $input['lastname'];
        $input['email'] = $input['email'];
        $input['password'] = bcrypt($input['password']);

        User::create($input);

        return redirect('/');

    }

}