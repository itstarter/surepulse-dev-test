<br /><br /><br />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
                <h3>Simple Registration</h3>
                <br />



                 <form class="form-horizontal" role="form" method="POST" action="{{ action('RegistrationController@processAddUser') }}" >
                {!! csrf_field() !!}
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" name="firstname"  id="firstname"  placeholder="Enter First Name ">
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Enter Last Name">
                    </div>
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" class="form-control" name="email" id="email"  placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password"  id="password" placeholder="Password">
                    </div>

                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" name="confirm_password"  id="confirm_password" placeholder="Confirm Password">
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
        </div>
    </div>
</div>
